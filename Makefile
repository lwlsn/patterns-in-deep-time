all: deeptimex.pdf deeptime.html

clean:
	rm deeptime.pdf deeptime.html

deeptime.html: deeptime.md references.bib
	pandoc --template=pandoc/iclc.html --citeproc --number-sections deeptime.md -o deeptime.html

deeptime.pdf: deeptime.md references.bib pandoc/iclc.latex pandoc/iclc.sty
	pandoc --template=pandoc/iclc.latex --citeproc --number-sections deeptime.md -o deeptime.pdf

deeptime.docx: deeptime.md references.bib
	pandoc --citeproc --number-sections deeptime.md -o deeptime.docx

deeptimex.pdf: deeptime.md references.bib pandoc/iclc.latex pandoc/iclc.sty
	pandoc --template=pandoc/iclc.latex --citeproc --number-sections deeptime.md --pdf-engine=xelatex -o deeptimex.pdf
