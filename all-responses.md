Iván Paz

a) How was your experience with the tablet weaving workshop? What was
complex, what was simple? How does this compare or contrast from your
experience with code?
I found more complexity in controlling the materiality of the "wool" than remembering the movements.
The weaving algorithms were clear in my head, but knowing the right tension, the right pressure and where to stop pulling was difficult at the beginning.
With the successive repetitions the movements felt more natural.  

b) Here's an from excerpt Joanne Armitage's paper "Spaces to Fail in:
Negotiating Gender, Community and Technology in Algorave"

"For some, code emerges as a way of dealing with or organising life,
for others code allows an expression of self, or a way of manipulating
lived experiences and speaking back to them creatively. One person
interviewed spoke about code as a way of working through their daily
life, adding structures to it and providing functions for being. These
lived patterns merge with their daydreams and expressions of colour
and geometry to form her live coded visuals."

How does this relate to your life? Can you share an experience that
compares or contrasts with it?
In terms of sound, code and repetition have always been a way to structure the daily inputs of life.
Repetitive sounds are easier to recognize, as it they allow me to pay attention to them; as I am writing, someone is pulling a some sort of shopping cart by the street.
And it is like the sound of the wheels and the pavement speak by themselves, unveiling its shape. 

c) Do you enjoy a patterny craft or other patterny activity? e.g.
weaving, braiding, origami, juggling, etc.. If so what does live
coding and this activity give you that compares, and where do they
diverge?
They diverge in its matter, the physical matter on one hand and the representation on the other. I had the experience of building origami balls while studying maths, but the balls




![](origami-ball.png)
but in the balls each part was a conscious repetition but the pattern appeared only as the ensemble.
To me weaving feels more natural, I barely had the weaving experience before.  

d) Have you thought about the workshop in the last few months, and if
so what about it has stayed with you? Any influences on your thinking
or makings?
I think I loved the conscious experience of following an algorithm, understanding it to the point that I can almost predict the result of a small variation,
this has offered me a different experience of visualizing the algorithms that I normally use in maths, as if the process that they described had a more material presence in the physical time and space.

Lizzie Wilson

a) How was your experience with the tablet weaving workshop? What was
complex, what was simple? How does this compare or contrast from your
experience with code?

The tablet weaving workshop was a great way for live coders to step away from the computer and contextualise what it means to be a live coder or a pattern maker, or both. I have been wanting to do some more physical pattern making for a while and found this a very interesting starting place. The fact the machines were made entirely from cardboard and whatever you tied it to, gave it a lower barrier to entry for those who wanted to just pick it up and try it out. 

I found myself getting quite obsessed with it quite quickly. Although I don’t always prefer physical to cognitive practices, there was something quite enchanting about working with your hands and watching the patterns begin to appear. I appreciated the slowness of it – of spending some time together with the weave and feeling like we were creating something together. That’s how I feel when I code too, but everything is always so instantaneous. 

As with coding, some complexity of the system began to arise when errors started to occur. It was relatively easy to undo sometimes, but there did seem times when small perturbations from what the instructor was doing felt that it shifted the outcome quite far.  As with coding though, this did sometimes produce surprising and unexpected results that forced me to engage creatively with the weaving process. 



b) Here's an from excerpt Joanne Armitage's paper "Spaces to Fail in:
Negotiating Gender, Community and Technology in Algorave"

"For some, code emerges as a way of dealing with or organising life,
for others code allows an expression of self, or a way of manipulating
lived experiences and speaking back to them creatively. One person
interviewed spoke about code as a way of working through their daily
life, adding structures to it and providing functions for being. These
lived patterns merge with their daydreams and expressions of colour
and geometry to form her live coded visuals."

How does this relate to your life? Can you share an experience that
compares or contrasts with it?

This quote resonates with what we did in the workshop. Seeing pattern as something that exists and ties together multiple disciplines, and also that is a tool for structuring and navigating our lives. 





c) Do you enjoy a patterny craft or other patterny activity? e.g.
weaving, braiding, origami, juggling, etc.. If so what does live
coding and this activity give you that compares, and where do they
diverge?

I got into origami quite a bit when I was younger, and see it as quite similar to my practice as a musician. I like how repetitions, shifts etc come together to produce things that can be exciting and surprising and I like when you can’t figure something out straight away and that it takes time to master.


d) Have you thought about the workshop in the last few months, and if
so what about it has stayed with you? Any influences on your thinking
or makings?

As I mentioned, I found I got obsessed with it quite quickly. I really liked the accompanying computational representation that was going on, and tried to do a code representation myself to help try and parse what was happening... 

 

I also liked looking at the examples and seeing the way different cultures had their own representations of pattern that convey different meanings. It made me think about how music also conveys cultural meaning, and I wondered if there was any way of connecting these ideas of representation to musical representations (e.g. scores).  
				 
roger    rogerpibernat@gmail.com    

It was challenging, but that made it interesting. It's something that feels like it can be mastered with dedication, just like code. The patterns that came out were really surprising, and it did feel a bit like getting unexpected results from code. The complex results out from simple patterned moves was really satisfying.    

I just realized I never stopped to consider what role does code play in my life. I'm not specially fond of finding patterns everywhere, I like exceptions. And recursiveness, which -in code- leads to very interesting outcomes. Maybe what I like about code is that it allows me to twist logic into poetic ways, which probably could be called a means of expression. I also use it to simplify my working workflow, I build and tweak software tools to make my life easier.  In live coding music, I am surprised to have found a way to express my musicality quite fluently.    

I lived in a boat for a few years and got to use knots to create all kinds of stuff, both useful and decoratively useless. I really enjoyed it but haven't done anything since I moved inland. I find code to be more of a problem-solving activity, while "knotting" felt more like a meditative one. Both, though, are very satisfying on their outcomes, make one feel fulfilled.    

I have come across other weaving people that brought me back to it. What really struck me and stuck with me was the complexity that can be achieved with the simplest of the tools and rules. I tried to port some of those ideas to my live coding music practice, but it eventually went away. I may get back to it, now that you mention it :)    			

Joana Chicau    web@joanachicau.com    

I enjoy the workshop, not only my individual activity but also the fact that I was surrounding by others doing the same. There was a sense of collectiveness. I could feel the energy in the room, observe, learn from others and also help at times. I found the "hands-on" and movement focused character of weaving a smooth way of engaging in pattern making. For me muscle memory helps me a lot in making, at the start I would rely on visuals cues, like how many degrees have I rotated the tablets, then, overtime, it became intuitive and fairly quickly I managed to improvise new patterns and explore more interesting combinations. A challenging part is how to keep the thread tidy and with the right tension, but even that didn't overshadowed the process or fun!    

I can relate to Joanne's quote. There is a logic to coding that can be expressed in many ways, independent from computing. For example, instruction based languages are present in daily life. For me personally, one manifestation is in choreography and in thinking of ways to '(dis—)organise' movement.     

I have done various of the activities mentioned, movement patterns are the ones I am mostly interested in. Such as those found in choreographic and dance practices, but also more broadly.    The exercise when people were asked to choose their favourite pattern and then pass it on to the next person to code in their own preferred language / software was interesting. That stayed with me, this idea of a collective string within which patterns are passed on 'hand-in-hand' , reinterpreted and creating a lineage of patterns.   	

Eloi Isern    nuntxaku@gmail.com


It was, above all, a lot of fun. For someone like me, who does not come from a computer background, it was amazing to see the possibilities that opened up in working with the loom.    

 "My way of using code, and specifically Tidal, is based on writing large blocks full of randomized processes. Very often I run these blocks of processes and I can be listening to the ever-changing sound output for several minutes, without doing anything. Just listening to what the code is proposing to me. This is a highly satisfying feeling. However, if I record the result and listen to it, I can't help but edit it and cut out bits and pieces that on first listen I thought were brilliant. Even today I still don't understand why I do this. Translated with www.DeepL.com/Translator (free version)"    

I just pattern when I code    

Given the way I write, with long blocks of code and randomizations within randomizations, I am always looking for new ways to work with the functions that Tidal offers and new logics. However, I am not a computer or programming professional, so my abstract thinking capabilities in this field are limited.    				 	


Flor de Fuego floralonsoflor@gmail.com

It was complex but fun. Compare to code I can see it takes more time and another physycal effort.   

I also liked the newness, the mapping between what I was doing physically and what was coming out of the weave wasn’t always clear at first, but the more I navigated through the weave the more things started to become apparent. 
I totally can relate code with daily life, code can be very structure and organize but also chaotic and unpreditable. I try to look for code as an expressive tool for communicating and connection with different disciplines."

No, not really but my background is drawing at it was my first experience and contact with programming somehow it was the first thing I could find a relation with visual programming.

Yes, I thought a lot about a very ancient weaving that was on the slides. I remember Dave saying something like this was a message that had thousands of years distance. And the idea of a weaving as a message, which I already somehow was aware because in Argentina we have that kind of idea with traditional weaving. But I just never really thought about it in depth because I never really payed much attention to it, so it was blowmind to connect everything.

Timo Hoogland

a) How was your experience with the tablet weaving workshop? What was
complex, what was simple? How does this compare or contrast from your
experience with code?

For me the “trial-and-error” approach worked pretty well. Trying out different rotations of the tablets and repeating my randomly thought-of algorithms to see what the pattern is that emerges over time. In some ways it fits my approach to programming music and visuals, where I can have an idea of an algorithm I would like to explore, starting with the “what if…?” question, and then see what happens from there over time. On the other hand, when programming music, I can make an educated guess on what I can expect to happen, while with the tablet weaving this was not so much the case since I was completely new to it. This resulted for me in some interesting surprises of patterns that came out. When Dave showed the website, where you can program the patterns online and see the results, a part of me wanted to first design the algorithm and then make it in real-life, error-free and perfect. But we actually didn’t have so much time to explore the web-tool so I stayed in the on-the-fly approach.

c) Do you enjoy a patterny craft or other patterny activity? e.g.
weaving, braiding, origami, juggling, etc.. If so what does live
coding and this activity give you that compares, and where do they
diverge?

During the lockdown I also joined an origami workshop, which I enjoyed very much as well. I don’t really do any of these crafts myself and basically only work with the computer for making digital art, but it is great to get away from the screen now-and-then. Similar as with live coding I enjoy the fact that with these crafts you’re also making things in the moment, getting real time feedback from what you are making and not really being able to undo. Although with weaving there is a little bit of “latency” where it can take a few steps for a pattern to reveal itself.

d) Have you thought about the workshop in the last few months, and if
so what about it has stayed with you? Any influences on your thinking
or makings?

Since the workshop I have included weaving and origami as an alternative example of pattern making in my introduction class on algorithmic composition and computational creativity that I teach at HKU University of the Arts Utrecht to Music Technology first year bachelor students.

In an earlier mail I also send some images of my result during the workshop, here are those photos again, feel free to use them if you like for a compilation of results or something: https://ln5.sync.com/dl/b8b75f510/5b352d95-jzjzcb9i-b7vwkpqg-dwc7yfab
